Feature: SignUp into losestudiantes
    As an user I want to register myself within losestudiantes website in order to rate teachers

    Scenario Outline: SignUp Failed - Account is already exist
        Given I go to losestudiantes home screen
        When I click on the element "button=Ingresar"
        And I enter "<nombre>" into the input field "input[name='nombre']"
        And I enter "<apellido>" into the input field "input[name='apellido']"
        And I enter "<correo>" into the input field "input[name='correo']"
        And I click on the element "label*=maestria"
        And I select the option "<programa>" for the list "select[name='idPrograma']"
        And I enter "<password>" into the input field "input[name='password']"
        And I click on the element "input[name='acepta']"
        And I click on the element "button=Registrarse"
        And I expect to see the element "//div[contains(@class,'sweet-alert')]/div/div" with text "<mensajeAlerta>"

        Examples:
            | nombre         | apellido       | correo                     | programa                           | password   | mensajeAlerta                                 |
            | Juan Sebastian | Castro Morales | js.castrom@uniandes.edu.co | Maestría en Ingeniería de Software | 1234567890 | Ya existe un usuario registrado con el correo |

    Scenario Outline: SignUp Failed - Required input fields
        Given I go to losestudiantes home screen
        When I click on the element "button=Ingresar"
        And I enter "<nombre>" into the input field "input[name='nombre']"
        And I enter "<apellido>" into the input field "input[name='apellido']"
        And I enter "<correo>" into the input field "input[name='correo']"
        And I click on the element "label*=maestria"
        And I select the option "<programa>" for the list "select[name='idPrograma']"
        And I enter "<password>" into the input field "input[name='password']"
        And I click on the element "input[name='acepta']"
        And I click on the element "button=Registrarse"
        And I expect to see the element "<iconoRequerido>"

        Examples:
            | nombre         | apellido       | correo                     | programa                           | password   | iconoRequerido                                                  |
            | null           | Castro Morales | js.castrom@uniandes.edu.co | Maestría en Ingeniería de Software | 1234567890 | //fieldset[1]/div/div/span[contains(@class,'glyphicon-remove')] |
            | Juan Sebastian | null           | js.castrom@uniandes.edu.co | Maestría en Ingeniería de Software | 1234567890 | //fieldset[2]/div/div/span[contains(@class,'glyphicon-remove')] |
            | Juan Sebastian | Castro Morales | null                       | Maestría en Ingeniería de Software | 1234567890 | //fieldset[3]/div/div/span[contains(@class,'glyphicon-remove')] |
            | Juan Sebastian | Castro Morales | js.castrom@uniandes.edu.co | null                               | 1234567890 | //fieldset/div/span[contains(@class,'glyphicon-remove')]     |
            | Juan Sebastian | Castro Morales | js.castrom@uniandes.edu.co | Maestría en Ingeniería de Software | null       | //fieldset[5]/div/div/span[contains(@class,'glyphicon-remove')] |

    Scenario: SignUp Failed - Accept the terms
        Given I go to losestudiantes home screen
        When I click on the element "button=Ingresar"
        And I enter "Juan Sebastian" into the input field "input[name='nombre']"
        And I enter "Castro Morales" into the input field "input[name='apellido']"
        And I enter "js.castrom@uniandes.edu.co" into the input field "input[name='correo']"
        And I click on the element "label*=maestria"
        #And I select the option "Maestría en Ingeniería de Software" for the list "select[name='idPrograma']"
        And I enter "1234567890" into the input field "input[name='password']"
        And I click on the element "button=Registrarse"
        And I expect to see the element "div[role='alert']" with text "Debes aceptar los términos"

    Scenario Outline: SignUp Successfull Undergraduate
        Given I go to losestudiantes home screen
        When I click on the element "button=Ingresar"
        And I enter "<nombre>" into the input field "input[name='nombre']"
        And I enter "<apellido>" into the input field "input[name='apellido']"
        And I enter "<correo>" into the input field "input[name='correo']"
        And I select the option "<programa>" for the list "select[name='idPrograma']"
        And I enter "<password>" into the input field "input[name='password']"
        And I click on the element "input[name='acepta']"
        And I click on the element "button=Registrarse"
        And I expect to see the element "//div[contains(@class,'sweet-alert')]/h2" with text "<mensajeAlerta>"

        Examples:
            | nombre         | apellido       | correo           | programa       | password   | mensajeAlerta     |
            | Juan Sebastian | Castro Morales | asd123@gmail.com | Administración | 1234567890 | Registro exitoso! |

    Scenario Outline: SignUp Successfull Postgraduate
        Given I go to losestudiantes home screen
        When I click on the element "button=Ingresar"
        And I enter "<nombre>" into the input field "input[name='nombre']"
        And I enter "<apellido>" into the input field "input[name='apellido']"
        And I enter "<correo>" into the input field "input[name='correo']"
        And I click on the element "label*=maestria"
        And I select the option "<programa>" for the list "select[name='idPrograma']"
        And I enter "<password>" into the input field "input[name='password']"
        And I click on the element "input[name='acepta']"
        And I click on the element "button=Registrarse"
        And I expect to see the element "//div[contains(@class,'sweet-alert')]/h2" with text "<mensajeAlerta>"

        Examples:
            | nombre         | apellido       | correo           | programa                           | password   | mensajeAlerta     |
            | Juan Sebastian | Castro Morales | fgh456@gmail.com | Maestría en Ingeniería de Software | 1234567890 | Registro exitoso! |