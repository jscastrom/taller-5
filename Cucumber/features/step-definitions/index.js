var { defineSupportCode } = require('cucumber');
var { expect } = require('chai');

defineSupportCode(({ Given, When, Then }) => {
    Given('I go to losestudiantes home screen', () => {
        browser.url('https://losestudiantes.co/');
        if (browser.isVisible('button=Cerrar')) {
            browser.click('button=Cerrar');
        }
    });

    When('I open the login screen', () => {
        browser.waitForVisible('button=Ingresar', 5000);
        browser.click('button=Ingresar');
    });

    When('I try to login', () => {
        var cajaLogIn = browser.element('.cajaLogIn');
        cajaLogIn.waitForVisible('button=Ingresar', 5000);
        cajaLogIn.element('button=Ingresar').click()
    });

    When(/^I fill with (.*) and (.*)$/, (email, password) => {
        var cajaLogIn = browser.element('.cajaLogIn');

        browser.waitForVisible('input[name="correo"]', 5000);
        var mailInput = cajaLogIn.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys(email);

        var passwordInput = cajaLogIn.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys(password)
    });

    Then(/^I expect to see "([^"]*)?"$/, (error) => {
        browser.waitForVisible('.aviso.alert.alert-danger', 5000);
        var alertText = browser.element('.aviso.alert.alert-danger').getText();
        expect(alertText).to.include(error);
    });

    Then('I expect to be able to login', () => {
        browser.waitForVisible('button[id="cuenta"]', 5000);
    });




    Given(/^I go to "([^"]*)?" page$/, (page) => {
        browser.url(page);
    });

    When(/^I click on the element "([^"]*)?"$/, (keyElement) => {
        browser.waitForVisible(keyElement, 5000);
        browser.click(keyElement);
    });
    
    When(/^I enter "([^"]*)?" into the input field "([^"]*)?"$/, (text, keyElement) => {
        browser.waitForVisible(keyElement, 5000);
        var inputField = browser.element(keyElement);
        inputField.click();
        if (text.toUpperCase() != "NULL") {
            inputField.keys(text);
        }
    });

    When(/^I select the option "([^"]*)?" for the list "([^"]*)?"$/, (option, keyElement) => {
        browser.waitForVisible(keyElement, 5000);
        var selectBox = browser.element(keyElement);
        if (option.toUpperCase() != "NULL") {
            selectBox.selectByVisibleText(option);
        }
    });

    Then(/^I expect to see the element "([^"]*)?" with text "([^"]*)?"$/, (keyElement, text) => {
        browser.waitForVisible(keyElement, 5000);
        var alertText = browser.element(keyElement).getText();
        expect(alertText).to.include(text);
    });

    Then(/^I expect to see the element "([^"]*)?"$/, (keyElement) => {
        browser.waitForVisible(keyElement, 5000);
    });
});